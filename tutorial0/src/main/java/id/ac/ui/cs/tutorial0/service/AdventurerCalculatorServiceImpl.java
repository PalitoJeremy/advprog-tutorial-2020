package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public String countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        int tempPower;
        if (rawAge<30) {
            tempPower = rawAge*2000;
        } else if (rawAge <50) {
            tempPower = rawAge*2250;
        } else {
            tempPower = rawAge*5000;
        }
        if (tempPower < 20000) {
            return Integer.toString(tempPower) + " C Class";
        } else if (tempPower >= 20000 && tempPower < 100000){
            return Integer.toString(tempPower) + " B Class";
        } else {
            return Integer.toString(tempPower) + " A Class";
        }
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
